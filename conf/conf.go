// conf
// 配置文件位置：{ExecPath}/conf.toml
package conf

import (
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/spf13/viper"
)

var _conf *viper.Viper

func init() {
	_conf = viper.New()
	_conf.AddConfigPath(ExecPath())
	_conf.AddConfigPath("./")
	_conf.SetConfigName("conf")
	_conf.SetConfigType("toml")
	err := _conf.ReadInConfig()
	if err != nil {
		panic(err)
	}

	_conf.WatchConfig()
}

// ExecPath 获取程序路径
// e.g: c:/ c:/program/
func ExecPath() string {
	ex, err := os.Executable()
	if err != nil {
		return "./"
	}
	exPath := filepath.Dir(ex)
	realPath, err := filepath.EvalSymlinks(exPath)
	if err != nil {
		return "./"
	}
	if runtime.GOOS == "windows" {
		realPath = strings.Replace(realPath, "\\", "/", -1)
	}

	if realPath[len(realPath)-1] != '/' {
		realPath = realPath + "/"
	}

	return realPath
}

func IsSet(key string) bool {
	return _conf.IsSet(key)
}

func Get(key string) any {
	return _conf.Get(key)
}

func GetBool(key string) bool {
	return _conf.GetBool(key)
}

func GetInt(key string) int {
	return _conf.GetInt(key)
}

func GetFloat32(key string) float32 {
	return float32(_conf.GetFloat64(key))
}

func GetFloat64(key string) float64 {
	return _conf.GetFloat64(key)
}

func GetString(key string) string {
	return _conf.GetString(key)
}

func GetTime(key string) time.Time {
	return _conf.GetTime(key)
}

func GetStringSlice(key string) []string {
	return _conf.GetStringSlice(key)
}

func GetIntSlice(key string) []int {
	return _conf.GetIntSlice(key)
}

func GetDuration(key string) time.Duration {
	return _conf.GetDuration(key)
}
