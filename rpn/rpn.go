// rpn Reverse Polish Notation
package rpn

import (
	"errors"
	"strconv"
)

const maxBuf int = 1024

type stack struct {
	buf  [maxBuf]string
	size int
}

func (s *stack) Empty() bool {
	return s.size == 0
}
func (s *stack) Full() bool {
	return s.size == maxBuf
}

func (s *stack) Size() int {
	return s.size
}

func (s *stack) Top() string {
	if s.size == 0 {
		return ""
	}
	return s.buf[s.size-1]
}

func (s *stack) Pop() string {
	if s.size == 0 {
		return ""
	}
	s.size--
	return s.buf[s.size]
}

func (s *stack) Push(item string) {
	if s.size == maxBuf {
		return
	}
	s.buf[s.size] = item
	s.size++
}

func opr_level(opr string) int {
	switch opr {
	case "+", "-":
		return 1
	case "*", "/" /*, "%"*/ :
		return 2
	case "(":
		fallthrough
	default:
		return 0
	}
}

// Trans2RPN translate expression to "Reverse Polish Notation"
func Trans2RPN(exp string) []string {
	strTmp := ""
	rpnExp := make([]string, 0, 128)
	oprStack := &stack{}

	for _, v := range exp {
		if v == ' ' || v == '	' {
			continue
		}
		switch v {
		case '(':
			if strTmp != "" {
				rpnExp = append(rpnExp, strTmp)
				strTmp = ""
			}
			oprStack.Push(string(v))
		case ')':
			if strTmp != "" {
				rpnExp = append(rpnExp, strTmp)
				strTmp = ""
			}
			str := ""
			for !oprStack.Empty() {
				str = oprStack.Pop()
				if str == "(" {
					break
				}
				rpnExp = append(rpnExp, str)
			}
		case '+', '-', '*', '/' /*, '%'*/ :
			if strTmp != "" {
				rpnExp = append(rpnExp, strTmp)
				strTmp = ""
			}
			for !oprStack.Empty() {
				if opr_level(oprStack.Top()) >= opr_level(string(v)) {
					rpnExp = append(rpnExp, oprStack.Pop())
				} else {
					break
				}
			}
			oprStack.Push(string(v))
		default:
			strTmp += string(v)
		}
	}

	if strTmp != "" {
		rpnExp = append(rpnExp, strTmp)
		strTmp = ""
	}

	for !oprStack.Empty() {
		rpnExp = append(rpnExp, oprStack.Pop())
	}
	return rpnExp
}

// Calc calculate expression results(float64)
// support operator: +,-,*,/,()
func Calc(exp string) (string, error) {
	rpnExp := Trans2RPN(exp)
	cStack := &stack{}

	for _, v := range rpnExp {
		if v == "+" || v == "-" || v == "*" || v == "/" {
			if cStack.Empty() {
				return "", errors.New("invalid expression: " + exp)
			}
			num2, err := strconv.ParseFloat(cStack.Pop(), 64)
			if err != nil {
				return "", errors.New("invalid expression: " + exp)
			}

			if cStack.Empty() {
				return "", errors.New("invalid expression: " + exp)
			}
			num1, err := strconv.ParseFloat(cStack.Pop(), 64)
			if err != nil {
				return "", errors.New("invalid expression: " + exp)
			}

			var rst float64 = 0
			switch v {
			case "+":
				rst = num1 + num2
			case "-":
				rst = num1 - num2
			case "*":
				rst = num1 * num2
			case "/":
				rst = num1 / num2
			// case "%":
			// 	rst = num1 % num2
			default:
				return "", errors.New("invalid Operator: " + v)
			}
			cStack.Push(strconv.FormatFloat(rst, 'f', 4, 64))
		} else {
			cStack.Push(v)
		}
	}

	return cStack.Top(), nil
}
