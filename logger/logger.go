// logger.go
package logger

import (
	"bufio"
	"os"
	"sync"
	"time"
)

type LogFileWrite struct {
	path       string
	prName     string
	lock       sync.Mutex
	curFd      *os.File
	bw         *bufio.Writer
	maxBackups int
	maxSize    int
	//byDay or by size
	isBySize bool
}

var (
	defaultLogPath = "./logs"
	preName        = ""
)

func NewLogger(options ...OptionFn) *LogFileWrite {
	lfw := &LogFileWrite{
		path:   defaultLogPath,
		prName: preName,
	}

	for _, opt := range options {
		opt(lfw)
	}

	_, err := os.Stat(lfw.path)
	if err != nil {
		if os.IsNotExist(err) {
			if err := os.MkdirAll(lfw.path, os.ModePerm); err != nil {
				panic(err)
			}
		} else {
			panic(err)
		}
	}

	fd, err := os.OpenFile(lfw.genLogFileName(), os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	lfw.curFd = fd
	lfw.bw = bufio.NewWriterSize(fd, 4096)
	return lfw
}

func (lwc *LogFileWrite) Write(p []byte) (int, error) {
	return lwc.writeLog(p)
}

func (lwc *LogFileWrite) Close() error {
	_ = lwc.bw.Flush()
	return lwc.curFd.Close()
}

func (lfw *LogFileWrite) genLogFileName() string {
	return lfw.path + "/" + lfw.prName + time.Now().Format("2006-01-02") + ".log"
}

func (lwc *LogFileWrite) writeLog(p []byte) (int, error) {
	lwc.lock.Lock()
	n, err := lwc.bw.Write(p)
	lwc.bw.Flush()
	lwc.lock.Unlock()
	return n, err
}
