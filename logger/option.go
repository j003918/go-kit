// option.go
package logger

type OptionFn func(*LogFileWrite)

func WithPath(path string) OptionFn {
	return func(lfw *LogFileWrite) {
		lfw.path = path
	}
}

func WithPreName(name string) OptionFn {
	return func(lfw *LogFileWrite) {
		lfw.prName = name
	}
}
