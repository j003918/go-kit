// dt
package dt

type DataType int

const (
	Int64 DataType = iota
	Float64
	String
)
