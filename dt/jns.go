// jns
package dt

import (
	"encoding/json"
	"errors"
	"strconv"
)

type JsonNumberString struct {
	Type     DataType
	IntVal   int64
	FloatVal float64
	StrVal   string
}

// 实现 json.Unmarshaller 接口
func (jns *JsonNumberString) UnmarshalJSON(value []byte) error {
	if value[0] == '"' {
		jns.Type = String
		return json.Unmarshal(value, &jns.StrVal)
	}

	err := json.Unmarshal(value, &jns.IntVal)
	if err == nil {
		jns.Type = Int64
		return err
	}

	err = json.Unmarshal(value, &jns.FloatVal)
	if err == nil {
		jns.Type = Float64
		return err
	}

	return errors.New("unsupported NumberString type")
}

// 实现 json.Marshaller 接口
func (jns JsonNumberString) MarshalJSON() ([]byte, error) {
	switch jns.Type {
	case Int64:
		return json.Marshal(jns.IntVal)
	case Float64:
		return json.Marshal(jns.FloatVal)
	case String:
		return json.Marshal(jns.StrVal)
	default:
		return []byte{}, errors.New("impossible NumberString.Type")
	}
}

func (jns JsonNumberString) String() string {
	switch jns.Type {
	case Int64:
		return strconv.FormatInt(jns.IntVal, 10)
	case Float64:
		return strconv.FormatFloat(jns.FloatVal, 'f', 5, 64)
	case String:
		return jns.StrVal
	default:
		return ""
	}
}
