package jwt

import (
	"time"

	"github.com/golang-jwt/jwt/v5"
)

var defaultKey []byte = []byte("sd3,4g.<jmm@$%YD()#:·~》^")

type customClaims struct {
	UserData map[string]any `json:"user_data"`
	jwt.RegisteredClaims
}

// SignedToken
// key if key is nil, use the default key
// exp ExpiresAt time.Now().Add(exp)
func SignedToken(key []byte, exp time.Duration, userPubData map[string]any) (string, error) {
	claims := customClaims{
		userPubData,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(exp)),
			IssuedAt:  jwt.NewNumericDate(time.Now()),
			NotBefore: jwt.NewNumericDate(time.Now()),
			Issuer:    "jhf@RaybornTech",
			Subject:   "subject",
			//ID:        "1",
			//Audience:  []string{"somebody_else"},
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	if key == nil {
		return token.SignedString(defaultKey)
	}
	return token.SignedString(key)
}

// VerifyToken
// key if key is nil, use the default key
func VerifyToken(key []byte, authToken string) (map[string]any, error) {
	cc := &customClaims{}
	token, err := jwt.ParseWithClaims(authToken, cc, func(token *jwt.Token) (any, error) {
		if key == nil {
			return defaultKey, nil
		}
		return key, nil
	})

	if err != nil {
		return nil, err
	}

	if token.Valid {
		return cc.UserData, nil
	} else {
		return nil, err
	}

	// if token.Valid {
	// 	return cc.CustomData, nil
	// } else if errors.Is(err, jwt.ErrTokenMalformed) {
	// 	return nil, jwt.ErrTokenMalformed
	// } else if errors.Is(err, jwt.ErrTokenExpired) || errors.Is(err, jwt.ErrTokenNotValidYet) {
	// 	return nil, jwt.ErrTokenExpired
	// } else {
	// 	return nil, err
	// }
}
