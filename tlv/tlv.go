// tlv.go

// BigEndian
package tlv

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
)

type TLV struct {
	Tag    uint32
	Length uint32
	Value  []byte
}

func (tlv *TLV) Encode() ([]byte, error) {
	buf := bytes.NewBuffer([]byte{})

	tlv.Length = uint32(len(tlv.Value))

	if err := binary.Write(buf, binary.BigEndian, tlv.Tag); err != nil {
		return nil, fmt.Errorf("tlv tag encode error:%w", err)
	}

	if err := binary.Write(buf, binary.BigEndian, tlv.Length); err != nil {
		return nil, fmt.Errorf("tlv length encode error:%w", err)
	}

	n, err := buf.Write(tlv.Value)
	if err != nil {
		return nil, fmt.Errorf("tlv value encode error:%w", err)
	}

	if n != int(tlv.Length) {
		return nil, fmt.Errorf("tlv write buf len not eq tlv.Length")
	}

	return buf.Bytes(), nil
}

func (tlv *TLV) Decode(data []byte) error {
	buf := bytes.NewBuffer(data)

	if err := binary.Read(buf, binary.BigEndian, &tlv.Tag); err != nil {
		return fmt.Errorf("tlv tag decode error:%w", err)
	}

	if err := binary.Read(buf, binary.BigEndian, &tlv.Length); err != nil {
		return fmt.Errorf("tlv length decode error:%w", err)
	}

	tlv.Value = make([]byte, tlv.Length)
	n, err := buf.Read(tlv.Value)

	if err != nil {
		return fmt.Errorf("tlv value decode error:%w", err)
	}

	if uint32(n) != tlv.Length {
		return errors.New("tlv value decode error: tlv.Length not eq buf.Read() len")
	}

	return nil
}

// IntToBytesBigEndian
// n        int64<--(uint8,int8,int16,uint16,int32,uint32,int64,uint64)
// bytesLen 1:uint8|int8,2:uint16|int16,4:uint32|int32,8:uint64|int64
func IntToBytesBigEndian(n int64, bytesLen byte) ([]byte, error) {
	switch bytesLen {
	case 1:
		return []byte{uint8(n)}, nil
	case 2:
		return []byte{uint8(n >> 8), uint8(n)}, nil
	case 4:
		return []byte{uint8(n >> 24), uint8(n >> 16), uint8(n >> 8), uint8(n)}, nil
	case 8:
		return []byte{uint8(n >> 56), uint8(n >> 48), uint8(n >> 40), uint8(n >> 32), uint8(n >> 24), uint8(n >> 16), uint8(n >> 8), uint8(n)}, nil
	}

	return nil, fmt.Errorf("param bytesLen is invaild.")
}

// IntToBytesLittleEndian
// n        int64<--(uint8,int8,int16,uint16,int32,uint32,int64,uint64)
// bytesLen 1:uint8|int8,2:uint16|int16,4:uint32|int32,8:uint64|int64
func IntToBytesLittleEndian(n int64, bytesLen byte) ([]byte, error) {
	switch bytesLen {
	case 1:
		return []byte{uint8(n)}, nil
	case 2:
		return []byte{uint8(n), uint8(n >> 8)}, nil
	case 4:
		return []byte{uint8(n), uint8(n >> 8), uint8(n >> 16), uint8(n >> 24)}, nil
	case 8:
		return []byte{uint8(n), uint8(n >> 8), uint8(n >> 16), uint8(n >> 24), uint8(n >> 32), uint8(n >> 40), uint8(n >> 48), uint8(n >> 56)}, nil
	}

	return nil, fmt.Errorf("param bytesLen is invaild.")
}
