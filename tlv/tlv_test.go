// tlv_test.go
package tlv

import (
	//"slices"
	"testing"
)

func Test_Encode_Func1(t *testing.T) {
	strPayload := "0123456789ABC"
	tlv := TLV{}
	tlv.Tag = 0x01
	tlv.Value = []byte(strPayload)

	buf, err := tlv.Encode()
	if err != nil {
		t.Error(err)
	} else {
		t.Log("ok", buf)
	}
}

func Test_Decode_Func1(t *testing.T) {
	strRst := "0123456789ABC"
	buf := []byte{0, 0, 0, 1, 0, 0, 0, 13, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67}
	tlv := TLV{}

	err := tlv.Decode(buf)

	if err != nil {
		t.Error(err)
		return
	}

	if tlv.Tag != 0x01 {
		t.Error("tvl.tag error")
		return
	}
	if tlv.Length != 13 {
		t.Error("tvl.length11 error")
		return
	}

	if string(tlv.Value) != strRst {
		t.Error("tvl.Value error")
		return
	}

	t.Log("ok", tlv)
}
