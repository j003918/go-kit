// office2pdf office2pdf_windows.go
package office2pdf

import (
	"errors"
	"os"

	"github.com/go-ole/go-ole"
	"github.com/go-ole/go-ole/oleutil"
)

func OfficeWord2Pdf(fileName string, pdfPath string) error {
	fi, err := os.Stat(fileName)
	if err != nil || fi.IsDir() {
		return errors.New("fileName:" + fileName + "error")
	}
	err = ole.CoInitialize(0)
	if err != nil {
		return err
	}
	defer ole.CoUninitialize()
	unknown, err := oleutil.CreateObject("Word.Application")
	if err != nil {
		return err
	}
	defer unknown.Release()
	word, err := unknown.QueryInterface(ole.IID_IDispatch)
	if err != nil {
		return err
	}
	defer word.Release()
	_, err = oleutil.PutProperty(word, "Visible", false)
	if err != nil {
		return err
	}
	documents := oleutil.MustGetProperty(word, "Documents").ToIDispatch()
	defer documents.Release()
	document := oleutil.MustCallMethod(documents, "Open", fileName).ToIDispatch()
	defer document.Release()
	oleutil.MustCallMethod(document, "SaveAs2", pdfPath, 17).ToIDispatch()
	oleutil.CallMethod(document, "Close")
	oleutil.CallMethod(word, "Quit")
	return nil
}

func OfficeExcel2Pdf(fileName string, pdfPath string) error {
	fi, err := os.Stat(fileName)
	if err != nil || fi.IsDir() {
		return errors.New("fileName:" + fileName + "error")
	}
	err = ole.CoInitialize(0)
	if err != nil {
		return err
	}
	defer ole.CoUninitialize()
	unknown, err := oleutil.CreateObject("Excel.Application")
	if err != nil {
		return err
	}
	defer unknown.Release()
	excel, err := unknown.QueryInterface(ole.IID_IDispatch)
	if err != nil {
		return err
	}
	defer excel.Release()
	_, err = oleutil.PutProperty(excel, "Visible", false)
	if err != nil {
		return err
	}
	workbooks := oleutil.MustGetProperty(excel, "Workbooks").ToIDispatch()
	defer workbooks.Release()
	workbook := oleutil.MustCallMethod(workbooks, "Open", fileName).ToIDispatch()
	defer workbook.Release()
	worksheet := oleutil.MustGetProperty(workbook, "Worksheets", 1).ToIDispatch()
	defer worksheet.Release()
	oleutil.MustCallMethod(worksheet, "ExportAsFixedFormat", 0, pdfPath).ToIDispatch()
	oleutil.CallMethod(workbook, "Close")
	oleutil.CallMethod(excel, "Quit")
	return nil
}

// func officePpt2pdf(fileName string, pdfPath string) {
// 	ole.CoInitializeEx(0, ole.COINIT_APARTMENTTHREADED)
// 	defer ole.CoUninitialize()
// 	unknown, _ := oleutil.CreateObject("PowerPoint.Application")
// 	defer unknown.Release()
// 	ppt, _ := unknown.QueryInterface(ole.IID_IDispatch)
// 	defer ppt.Release()
// 	presentations := oleutil.MustGetProperty(ppt, "Presentations").ToIDispatch()
// 	defer presentations.Release()
// 	presentation := oleutil.MustCallMethod(presentations, "Open", fileName).ToIDispatch()
// 	defer presentation.Release()
// 	oleutil.MustCallMethod(presentation, "SaveAs", pdfPath, 32).ToIDispatch()
// 	oleutil.CallMethod(presentation, "Close")
// 	oleutil.CallMethod(ppt, "Quit")
// }

func WpsWord2Pdf(fileName string, pdfPath string) error {
	fi, err := os.Stat(fileName)
	if err != nil || fi.IsDir() {
		return errors.New("fileName:" + fileName + "error")
	}
	err = ole.CoInitialize(0)
	if err != nil {
		return err
	}
	ole.CoInitialize(0)
	defer ole.CoUninitialize()
	unknown, err := oleutil.CreateObject("KWPS.Application")
	if err != nil {
		return err
	}
	defer unknown.Release()
	word, err := unknown.QueryInterface(ole.IID_IDispatch)
	if err != nil {
		return err
	}
	defer word.Release()
	_, err = oleutil.PutProperty(word, "Visible", false)
	if err != nil {
		return err
	}
	documents := oleutil.MustGetProperty(word, "Documents").ToIDispatch()
	defer documents.Release()
	document := oleutil.MustCallMethod(documents, "Open", fileName).ToIDispatch()
	defer document.Release()
	oleutil.MustCallMethod(document, "SaveAs", pdfPath, 16).ToIDispatch()
	oleutil.CallMethod(document, "Close")
	oleutil.CallMethod(word, "Quit")
	return nil
}

// func wpsPpt2pdf(fileName string, pdfPath string) {
// 	ole.CoInitialize(0)
// 	defer ole.CoUninitialize()
// 	unknown, _ := oleutil.CreateObject("KWPP.Application")
// 	defer unknown.Release()
// 	ppt, _ := unknown.QueryInterface(ole.IID_IDispatch)
// 	defer ppt.Release()
// 	oleutil.PutProperty(ppt, "Visible", false)
// 	presentations := oleutil.MustGetProperty(ppt, "Presentations").ToIDispatch()
// 	defer presentations.Release()
// 	presentation := oleutil.MustCallMethod(presentations, "Open", fileName).ToIDispatch()
// 	defer presentation.Release()
// 	oleutil.MustCallMethod(presentation, "SaveAs", pdfPath, 32).ToIDispatch()
// 	oleutil.CallMethod(presentation, "Close")
// 	oleutil.CallMethod(ppt, "Quit")
// }

func WpsExcel2Pdf(absInFileName string, absOutPdfFileName string) error {
	fi, err := os.Stat(absInFileName)
	if err != nil || fi.IsDir() {
		return errors.New("fileName:" + absInFileName + "error")
	}
	err = ole.CoInitialize(0)
	if err != nil {
		return err
	}
	defer ole.CoUninitialize()

	unknown, err := oleutil.CreateObject("KET.Application")
	if err != nil {
		return err
	}
	defer unknown.Release()
	excel, err := unknown.QueryInterface(ole.IID_IDispatch)
	if err != nil {
		return err
	}
	defer excel.Release()
	_, err = oleutil.PutProperty(excel, "Visible", false)
	if err != nil {
		return err
	}
	workbooks := oleutil.MustGetProperty(excel, "Workbooks").ToIDispatch()
	defer workbooks.Release()
	workbook := oleutil.MustCallMethod(workbooks, "Open", absInFileName).ToIDispatch()
	defer workbook.Release()
	worksheet := oleutil.MustGetProperty(workbook, "Worksheets", 1).ToIDispatch()
	defer worksheet.Release()
	oleutil.MustCallMethod(worksheet, "ExportAsFixedFormat", 0, absOutPdfFileName).ToIDispatch()
	oleutil.CallMethod(workbook, "Close")
	oleutil.CallMethod(excel, "Quit")
	return nil
}
