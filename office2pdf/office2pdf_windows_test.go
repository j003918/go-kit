// office2pdf_windows_test.go
package office2pdf

import (
	"testing"
)

func Test_OfficeWord2Pdf(t *testing.T) {
	srcFile := "D:\\test.doc"
	pdfFile := srcFile + ".pdf"
	err := OfficeWord2Pdf(srcFile, pdfFile)

	if err != nil {
		t.Error("NotPass:" + err.Error())
	} else {
		t.Log("PASS")
	}
}

func Test_OfficeExcel2Pdf(t *testing.T) {
	srcFile := "D:\\test.doc"
	pdfFile := srcFile + ".pdf"
	err := OfficeExcel2Pdf(srcFile, pdfFile)

	if err != nil {
		t.Error("NotPass:" + err.Error())
	} else {
		t.Log("PASS")
	}
}
