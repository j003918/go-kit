// pinyin
package charset

// GetPinYinHeader 获取拼音首字母(大写)，出错返回空值
// chinese 字符串(UTF8编码)
func GetPinYinHeader(chinese string) string {
	buf, err := FromUtf8([]byte(chinese), GB18030)
	if err != nil {
		return ""
	}

	var v int32
	bufLen := len(buf)
	strRst := ""
	for i := 0; i < bufLen; {
		if buf[i] >= 0 && buf[i] < 128 {
			strRst += string(buf[i])
			i++
			continue
		}

		v = int32(buf[i])<<8 | int32(buf[i+1])
		i = i + 2

		// 十进制落在GB2312的某个拼音区间即为某个字母
		charZone := v - 65535
		if charZone >= -20319 && charZone <= -20284 {
			strRst = strRst + "A"
			continue
		}
		if charZone >= -20283 && charZone <= -19776 {
			strRst = strRst + "B"
			continue
		}
		if charZone >= -19775 && charZone <= -19219 {
			strRst = strRst + "C"
			continue
		}
		if charZone >= -19218 && charZone <= -18711 {
			strRst = strRst + "D"
			continue
		}
		if charZone >= -18710 && charZone <= -18527 {
			strRst = strRst + "E"
			continue
		}
		if charZone >= -18526 && charZone <= -18240 {
			strRst = strRst + "F"
			continue
		}
		if charZone >= -18239 && charZone <= -17923 {
			strRst = strRst + "G"
			continue
		}
		if charZone >= -17922 && charZone <= -17418 {
			strRst = strRst + "H"
			continue
		}
		if charZone >= -17417 && charZone <= -16475 {
			strRst = strRst + "J"
			continue
		}
		if charZone >= -16474 && charZone <= -16213 {
			strRst = strRst + "K"
			continue
		}
		if charZone >= -16212 && charZone <= -15641 {
			strRst = strRst + "L"
			continue
		}
		if charZone >= -15640 && charZone <= -15166 {
			strRst = strRst + "M"
			continue
		}
		if charZone >= -15165 && charZone <= -14923 {
			strRst = strRst + "N"
			continue
		}
		if charZone >= -14922 && charZone <= -14915 {
			strRst = strRst + "O"
			continue
		}
		if charZone >= -14914 && charZone <= -14631 {
			strRst = strRst + "P"
			continue
		}
		if charZone >= -14630 && charZone <= -14150 {
			strRst = strRst + "Q"
			continue
		}
		if charZone >= -14149 && charZone <= -14091 {
			strRst = strRst + "R"
			continue
		}
		if charZone >= -14090 && charZone <= -13319 {
			strRst = strRst + "S"
			continue
		}
		if charZone >= -13318 && charZone <= -12839 {
			strRst = strRst + "T"
			continue
		}
		if charZone >= -12838 && charZone <= -12557 {
			strRst = strRst + "W"
			continue
		}
		if charZone >= -12556 && charZone <= -11848 {
			strRst = strRst + "X"
			continue
		}
		if charZone >= -11847 && charZone <= -11056 {
			strRst = strRst + "Y"
			continue
		}
		if charZone >= -11055 && charZone <= -10247 {
			strRst = strRst + "Z"
			continue
		}
	}
	return strRst
}
