package charset

import (
	"testing"
)

func Benchmark_charset_GetPinYinHeader(b *testing.B) {
	str_exp := "23江海峰重置34计时器，忽略前面的准备时间f343@#$$"
	b.ResetTimer() // 重置计时器，忽略前面的准备时间
	for n := 0; n < b.N; n++ {
		GetPinYinHeader(str_exp)
	}
}

func Test_charset_GetPinYinHeader_1(t *testing.T) {
	str_exp := "江海峰"
	str_chek := "JHF"
	str_rst := GetPinYinHeader(str_exp)
	if str_rst != str_chek {
		t.Error("not pass ")
	} else {
		t.Log("pass")
	}
}

func Test_charset_GetPinYinHeader_2(t *testing.T) {
	str_exp := "江23海5峰a"
	str_rst := "J23H5Fa"

	if str_rst != GetPinYinHeader(str_exp) {
		t.Error("not pass ")
	} else {
		t.Log("pass")
	}
}

func Test_charset_GetPinYinHeader_3(t *testing.T) {
	str_exp := "赵照诗"
	str_rst := "ZZS"

	if str_rst != GetPinYinHeader(str_exp) {
		t.Error("not pass ")
	} else {
		t.Log("pass")
	}
}

func Test_charset_GetPinYinHeader_4(t *testing.T) {
	str_exp := "a赵b照c诗123"
	str_rst := "aZbZcS123"

	if str_rst != GetPinYinHeader(str_exp) {
		t.Error("not pass ")
	} else {
		t.Log("pass")
	}
}
