// charset
package charset

import (
	"bytes"
	"fmt"
	"io"

	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
)

// 字符编码		Bom(Hex)
// UTF-8         EF BB BF
// UTF-16(BE)    FE FF
// UTF-16(LE)    FF FE
// UTF-32(BE)    00 00 FE FF
// UTF-32(LE)    FF FE 00 00
// GB-18030      84 31 95 33

type EncodType uint8

const (
	unknow EncodType = iota
	UTF8
	GB2312
	GBK
	GB18030
	HZGB2312
)

// ToUtf8 将指定编码内容转成UTF8
func ToUtf8(buf []byte, from EncodType) ([]byte, error) {
	if from == UTF8 {
		return buf, nil
	}
	var bufReader *transform.Reader
	switch from {
	case GB2312:
		fallthrough
	case GBK:
		bufReader = transform.NewReader(bytes.NewReader(buf), simplifiedchinese.GBK.NewDecoder())
	case GB18030:
		bufReader = transform.NewReader(bytes.NewReader(buf), simplifiedchinese.GB18030.NewDecoder())
	case HZGB2312:
		bufReader = transform.NewReader(bytes.NewReader(buf), simplifiedchinese.HZGB2312.NewDecoder())
	default:
		return nil, fmt.Errorf("unsupport charset")
	}

	return io.ReadAll(bufReader)
}

// FromUtf8 将UTF8编码内容转换成指定编码
func FromUtf8(buf []byte, to EncodType) ([]byte, error) {
	if to == UTF8 {
		return buf, nil
	}

	var bufReader *transform.Reader
	switch to {
	case GB2312:
		fallthrough
	case GBK:
		bufReader = transform.NewReader(bytes.NewReader(buf), simplifiedchinese.GBK.NewEncoder())
	case GB18030:
		bufReader = transform.NewReader(bytes.NewReader(buf), simplifiedchinese.GB18030.NewEncoder())
	case HZGB2312:
		bufReader = transform.NewReader(bytes.NewReader(buf), simplifiedchinese.HZGB2312.NewEncoder())
	default:
		return nil, fmt.Errorf("unsupport charset")
	}

	return io.ReadAll(bufReader)
}
