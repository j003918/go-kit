// dbutil
package dbutil

import (
	"database/sql"
	"fmt"
)

// Sql2Map convers query to []map[string]any
//
// That's a specialty of MySQL: you have to use prepared statements to get
// the native types. MySQL has two protocols, one transmits everything as
// text, the other as the "real" type. And that binary protocol is only
// used when you use prepared statements.
// The driver is pretty much powerless to enforce a protocol and the text
// protocol takes less resources on the server.
//
// This may help you:
//
// stmt, err := db.Prepare(sqlString)
// if err != nil { ...... }
// defer stmt.Close()
// rows, err := stmt.Query()
func Sql2Map(db *sql.DB, strSql string, args ...any) ([]map[string]any, error) {
	if db == nil {
		return nil, fmt.Errorf("db is nil")
	}

	stmt, err := db.Prepare(strSql)
	if err != nil {
		return nil, fmt.Errorf("db.Prepare(): ", err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(args...)
	if err != nil {
		return nil, fmt.Errorf("stmt.Query(): ", err)
	}
	defer rows.Close()

	columns, err := rows.Columns()
	if err != nil {
		return nil, fmt.Errorf("rows.Columns(): ", err)
	}

	cols := len(columns)
	tableData := make([]map[string]any, 0, 512)
	values := make([]any, cols)
	scanArgs := make([]any, cols)
	for i := 0; i < cols; i++ {
		scanArgs[i] = &values[i]
	}

	for rows.Next() {
		err := rows.Scan(scanArgs...)
		if err != nil {
			return nil, fmt.Errorf("rows.Scan(): ", err)
		}
		rowData := make(map[string]any)
		for i, col := range columns {
			var v any
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}
			rowData[col] = v
		}
		tableData = append(tableData, rowData)
	}
	return tableData, nil
}

// Rows2Map convers rows to []map[string]any
// caller call the function rows.Close()
func Rows2Map(rows *sql.Rows) ([]map[string]any, error) {
	columns, err := rows.Columns()
	if err != nil {
		return nil, fmt.Errorf("rows.Columns(): ", err)
	}

	cols := len(columns)
	tableData := make([]map[string]any, 0)
	values := make([]any, cols)
	scanArgs := make([]any, cols)
	for i := 0; i < cols; i++ {
		scanArgs[i] = &values[i]
	}

	for rows.Next() {
		rows.Scan(scanArgs...)
		if err != nil {
			return nil, fmt.Errorf("rows.Scan(): ", err)
		}
		rowData := make(map[string]any)
		for i, col := range columns {
			var v any
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}
			rowData[col] = v
		}
		tableData = append(tableData, rowData)
	}
	return tableData, nil
}
