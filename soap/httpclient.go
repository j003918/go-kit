// httpclient
package soap

import (
	"crypto/tls"
	"io"
	"net/http"
	"time"
)

var (
	defaultTransport = &http.Transport{
		Proxy:                 http.ProxyFromEnvironment,
		ForceAttemptHTTP2:     true,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
	}
)

// HttpDo
// tp if nil use the default http.Transport
func HttpDo(method, url string, headers map[string]string, payload io.Reader, tp *http.Transport) (*http.Response, error) {
	req, err := http.NewRequest(
		method,
		url,
		payload,
	)

	if err != nil {
		return nil, err
	}

	req.Header.Set("User-Agent", "customization")
	for k, v := range headers {
		req.Header.Set(k, v)
	}

	transport := tp
	if transport == nil {
		transport = defaultTransport
	}

	c := &http.Client{
		Transport: transport,
		Timeout:   30 * time.Second,
	}

	rsp, err := c.Do(req)
	if err != nil {
		return rsp, err
	}
	defer c.CloseIdleConnections()
	return rsp, err
}
