package soap

import (
	"bytes"
	"net/http"
)

type CDATA struct {
	Text string `xml:",cdata"`
}

// Soap11
func Soap11(url, action string, payload []byte, headers map[string]string) (*http.Response, error) {
	_headers := headers
	if _headers == nil {
		_headers = map[string]string{}
	}

	_headers["Content-Type"] = `text/xml;charset=UTF-8`
	_headers["SOAPAction"] = action
	return HttpDo("POST", url, _headers, bytes.NewReader(payload), nil)
}

// Soap12
func Soap12(url, action string, payload []byte, headers map[string]string) (*http.Response, error) {
	_headers := headers
	if _headers == nil {
		_headers = map[string]string{}
	}

	_headers["Content-Type"] = `application/soap+xml;charset=UTF-8;action=` + action
	return HttpDo("POST", url, _headers, bytes.NewReader(payload), nil)
}
